const url = 'https://collections.museumvictoria.com.au/api/species/'

function fetchById(speciesId) {
  // temp = url+speciesId;
  // document.getElementById("demo").innerHTML = temp;
  // $.getJSON('https://collections.museumvictoria.com.au/api/species/14379', function(result) {
  //           document.getElementById("demo").innerHTML = result.displayTitle;
  //           });
  //       });
  $.ajax({
    type: 'GET',
    url: url+speciesId,
    success: function(result) {
      let output = `<h3>${result.displayTitle}</h3></br>

      <div class="slideshow-container">

        <!-- Full-width images with number and caption text -->
        <div class="mySlides" style="display: block; max-height: 300px; overflow:hidden;">
          <img src=${result.media[0].small.uri} style="width:100%">
        </div>

        <div class="mySlides" style="max-height: 300px; overflow:hidden;">
          <img src=${result.media[1].small.uri} style="width:100%">
        </div>

        <div class="mySlides" style="max-height: 300px; overflow:hidden;">
          <img src=${result.media[2].small.uri} style="width:100%">
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)" style="color:purple;">&#10094;</a>
        <a class="next" onclick="plusSlides(1)" style="color:purple;">&#10095;</a>
      </div>
      <br>

      <!-- The dots/circles -->
      <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
      </div>
      `;
      output += `
        <ul>
          <li>Animal Type: ${result.animalType}</li>
          <li>Animal Subtype: ${result.animalSubType}</li>
          <li>Habitats: ${result.habitat}</li>
          <li>Distribution: ${result.distribution}</li>
          <li>Biology: ${result.biology}</li>
          <li>Description: ${result.generalDescription}</li>
        </ul>
      `
      document.getElementById("demo").innerHTML = output;
    }
  })
}
