// Initialize Firebase
var config = {
  apiKey: "AIzaSyCcqRmdhSilKBRg5YAceKNTNi0AsdydbwU",
  authDomain: "dr-wildlife.firebaseapp.com",
  databaseURL: "https://dr-wildlife.firebaseio.com",
  projectId: "dr-wildlife",
  storageBucket: "dr-wildlife.appspot.com",
  messagingSenderId: "47387387774"
};
firebase.initializeApp(config);



function initMap() {
  // The location of Uluru
  var uluru = {lat: -25.344, lng: 131.036};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  // var marker = new google.maps.Marker({position: uluru, map: map});
  var postlist = firebase.database().ref("Wildlife_Posts");
  postlist.on('value',function(snapshot) {
    snapshot.forEach(function(child) {
      child.forEach(function(fchild) {
        var fchilds = fchild.val();
        console.log(fchilds);
        var infowindow = new google.maps.InfoWindow({
          content: `<div><img src=${fchilds.imageUrl} style='width:100%;height:100%;'></div></br>
                    <b>${fchilds.wildlifeName}</b>
                    `,
          maxWidth: 120,
          maxHeight: 120
        });
        var marker = new google.maps.Marker({
          position: {lat:fchilds.latitude, lng: fchilds.longtitude},
          title: fchilds.wildlifeName,
          map: map
        });
        // google.maps.event.addListener(marker, 'click', function() {
        //   infowindow.open(map, marker);
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
        map.addListener('click', function() {
          infowindow.close();
        });
        marker.setMap(map);
      });
    });
  });
}
