// Initialize Firebase
var config = {
  apiKey: "AIzaSyCcqRmdhSilKBRg5YAceKNTNi0AsdydbwU",
  authDomain: "dr-wildlife.firebaseapp.com",
  databaseURL: "https://dr-wildlife.firebaseio.com",
  projectId: "dr-wildlife",
  storageBucket: "dr-wildlife.appspot.com",
  messagingSenderId: "47387387774"
};
firebase.initializeApp(config);



function initList() {
  var rankingList = [];
  var postlist = firebase.database().ref("Wildlife_Posts");

  function User(name, posts, image) {
    this.name = name;
    this.posts = posts;
    this.image = image;
  }

  postlist.on('value',function(snapshot) {
    var num = 0;
    snapshot.forEach(function(child) {
        var childKey = child.key;
        // console.log(childKey);
        var count = child.numChildren();
        var ref = firebase.database().ref("users").child(childKey);
        ref.once('value').then(function(snapshot) {
          var username = snapshot.val().username;
          var profileImage = snapshot.val().profileImage;
          var user = new User(username, count, profileImage);

          $("#scoreboard tbody").append(
            "<tr class='item'> <td class='column1'><img class='avatar' src='" +
              profileImage +
              "'/>" +
              username +
              "</td><td class='column2'>" +
              count +
              "</td></tr>"
          );
          num++;

          rankingList.push(user);
        });
    });
  });
  rankingList.sort();
  return rankingList;
}

console.log(initList());
