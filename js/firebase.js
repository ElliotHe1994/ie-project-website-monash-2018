// Initialize Firebase
var config = {
  apiKey: "AIzaSyCcqRmdhSilKBRg5YAceKNTNi0AsdydbwU",
  authDomain: "dr-wildlife.firebaseapp.com",
  databaseURL: "https://dr-wildlife.firebaseio.com",
  projectId: "dr-wildlife",
  storageBucket: "dr-wildlife.appspot.com",
  messagingSenderId: "47387387774"
};
firebase.initializeApp(config);



function initMap() {
  // The location of Uluru
  var uluru = {lat: -37.8810227, lng: 145.0436836};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 11, center: uluru});
  // The marker, positioned at Uluru
  // var marker = new google.maps.Marker({position: uluru, map: map});

  var cliniclist = firebase.database().ref("Wildlife_Clinic");
  cliniclist.on('value',function(snapshot) {
    snapshot.forEach(function(child) {
      var childs = child.val();
      console.log(childs);
      var infowindow = new google.maps.InfoWindow({
        content: `<b>${childs.name}</b></br>Address: ${childs.address}</br>Phone: ${childs.phone}`
      });
      var marker = new google.maps.Marker({
        position: {lat:childs.lat, lng: childs.long},
        title: childs.name,
        icon: 'images/clinicIcon.png',
        map:map
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
      map.addListener('click', function() {
        infowindow.close();
      });
      marker.setMap(map);
    });
  });
}
